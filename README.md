# etg-docker: ekleinod/todo.txt

## Quick reference

- **Maintained by:** [ekleinod](https://gitlab.com/etg-docker/todo.txt/)
- **Docker hub:**: [ekleinod/todo.txt](https://hub.docker.com/r/ekleinod/todo.txt)
- **Where to get help:** [gitlab](https://gitlab.com/etg-docker/todo.txt/), [issue tracker](https://gitlab.com/etg-docker/todo.txt/-/issues)


## Supported tags

- `1.0.0`, `latest`


## What is this image?

This is a docker image for todo.txt.
The image has the name `ekleinod/todo.txt`.

- <http://todotxt.org>
- <https://github.com/todotxt>

For the complete changelog, see [changelog.md](https://gitlab.com/etg-docker/todo.txt/-/blob/main/changelog.md)


## How to use the image

You can use the image as follows:

~~~ bash
$ docker run --rm --name <containername> --user "$(id -u):$(id -g)" --volume "${PWD}:/todo" ekleinod/todo.txt <command>
~~~

For a simple test showing the todo.txt help page, run

~~~ bash
$ docker run --rm --name todo.txt ekleinod/todo.txt todo shorthelp
~~~

You can see examples at work in the test folder.
Clone the repository or download the folder.
Call them as follows:

~~~ bash
$ cd test
$ ./test_help.sh
$ ./test_build.sh
$ ./test_version.sh
~~~


## Releases

The latest release will always be available with:

`ekleinod/todo.txt:latest`

Internal version number `ekleinod/todo.txt:<major>.<minor>.<patch>`

	Example: `ekleinod/todo.txt:1.0.0`


## Build locally

In order to build the image locally, clone the repository and call

~~~ bash
$ cd image
$ ./build_image.sh
~~~

## Git-Repository

The branching model regards to the stable mainline model described in <https://www.bitsnbites.eu/a-stable-mainline-branching-model-for-git/>.

This means, there is always a stable mainline, the main branch.
This branch ist always compileable and testable, both without errors.

Features are developed using feature branches.
Special feature branches are used (different from the mainline model) for finalizing releases.

Releases are created as branches of the mainline.
Additionally, each release is tagged, tags contain the patch and, if needed, additional identifiers, such as `rc1` or `beta`.

Patches are made in the according release branch.
Minor version changes get their own release branch.

## Copyright

Copyright 2023-2023 Ekkart Kleinod <ekleinod@edgesoft.de>

The program is distributed under the terms of the GNU General Public License, either version 3 of the License, or
any later version.

See COPYING for details.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
