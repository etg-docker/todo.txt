# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog] and this project adheres to [Semantic Versioning].


## [Unreleased]

- initial version

### Added

- todo.txt cli `2.12.0`


[Unreleased]: https://gitlab.com/etg-docker/todo.txt/-/commits/feature%2Ft1-init

[Keep a Changelog]: https://keepachangelog.com/en/1.1.0/
[Semantic Versioning]: https://semver.org/spec/v2.0.0.html
