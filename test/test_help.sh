source ../settings.sh

echo "Empty call - shows help."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}/example:/todo" \
	$IMAGENAME
