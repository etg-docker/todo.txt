source ../settings.sh

echo "List example todos."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}/example:/todo" \
	$IMAGENAME \
		list
