source ../settings.sh

echo "Shows version."
echo

docker run \
	--rm \
	--user "$(id -u):$(id -g)" \
	--name $CONTAINERNAME \
	--volume "${PWD}/example:/todo" \
	$IMAGENAME \
		-V
