#!/bin/bash

source ../settings.sh

echo "Build image $IMAGENAME"
echo

docker build --progress=plain --tag $IMAGENAME:$DOCKER_TAG_INTERNAL .
docker tag $IMAGENAME:$DOCKER_TAG_INTERNAL $IMAGENAME:$DOCKER_TAG_LATEST
