# Dockerfile

This `Dockerfile` has some complicated bits, so in order for me not to forget what I did...

## Get the latest release

~~~ shell
curl -silent $TODOTXT_REPO/releases/latest | \
	grep "browser_download_url.*.tar.gz" | \
	cut -d : -f 2,3 | \
	tr -d \" | \
	xargs curl --location | \
	tar -xzv -C $TODOTXT_INSTALL_DIR --strip-components 1 && \
~~~

This one gets the release information and looks for the line with `browser_download_url`.
Via magic (`cut` and `tr`) it extracts the download location.
This is piped into `curl` which downloads the release file.

Now `tar` can extract the files into the local destination folder.
It does so directly without the first subdirectory (`--strip-components 1`).

## Working directory

~~~ docker
WORKDIR $TODOTXT_WORKDIR
~~~

Defines the working directory.

In order for the cli to use it, it has to set as `TODO_DIR` environment variable, which is easiest to set in the config file in the `.todo` config directory:

~~~ shell
cp $TODOTXT_INSTALL_DIR/todo.cfg $TODOTXT_CONFIG_DIR/config && \
sed -i 's/export TODO_DIR=$(dirname "$0")/export TODO_DIR="\$TODOTXT_WORKDIR"/' $TODOTXT_CONFIG_DIR/config
~~~

1. copy the standard config file to the config dir, rename to `config`
2. replace the TODO_DIR default value with the working directory
